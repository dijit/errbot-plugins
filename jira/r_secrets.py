from os import path, getenv
import blimey

secrets = {
    "secrets": {
        "vault": 'Dragon01#',
        "sms_sid": 'AC025200f03dbc8fdb8f10667a0cfc7ad9',
        "sms_auth": 'a256c89c7f9b934747ece326139b2698',
        "sms_cid": '46769449025',
        "sms_oncall": '0046732408573',
        },
    "grafana": "eyJrIjoiT0VQY2laRnFaYjhVU1plNXhtSGFLQ25KbWs2R0RIYzQiLCJuIjoiemV0YSIsImlkIjoxfQ=="
    }


def return_windows_password() -> str:
    return return_secret('E78A6F95EF754AF8913D54503D8A1C41')

 
def return_secret(secret_id: str) -> str:
    if getenv('1PASS') is not None:
        AgilePath = getenv('1PASS')
    else:
        AgilePath = path.expanduser("~/Dropbox/Apps/1Password/1Password.agilekeychain")
    AGILEKEYCHAIN = blimey.AgileKeychain(AgilePath)
    AGILEKEYCHAIN.unlock(secrets['secrets']['vault'])
    ITEM = AGILEKEYCHAIN[secret_id]
    return ITEM.get('encrypted')['fields'][1]['value']

if __name__ == '__main__':
    print(return_windows_password())
