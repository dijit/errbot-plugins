from errbot import BotPlugin, botcmd, re_botcmd
from jira import JIRA
import re
import r_secrets

_JIRA_URL_MAP = {
        'HUNTER': 'https://mdc-tomcat-jira53.ubisoft.org/jira/',
        'SNOW': 'https://mdc-tomcat-jira53.ubisoft.org/jira/',
        'ROGUE': 'https://mdc-tomcat-jira60.ubisoft.org/jira/',
        'BLUE': 'https://msv-blue-jira.ubisoft.org/jira/',
        }


class JiraHelper(BotPlugin):
    def send_shit(self, msg, _issue: str) -> None:
        project = _issue.split('-')[0]
        try:
            jira = JIRA(
                    _JIRA_URL_MAP[project],
                    basic_auth=(
                        'jan.harasym@massive.se',
                        r_secrets.return_windows_password()
                        ),
                    options={'verify': False}
                    )
            try:
                issue = jira.issue(_issue)
                assigned = issue.fields.assignee.displayName
            except Exception:
                return "I couldn't find " + _issue + " in the archive."
        except Exception as e:
            return "Unable to get JIRA status"
        # created = issue.fields.created
        if issue.fields.issuetype.name == 'Epic':
            description = r'💮 EPIC'
        else:
            description = issue.fields.description or 'No Description'

        status = issue.fields.status.name or 'No Name'
        url = issue.permalink()
        title = issue.fields.summary or 'No Summary'
        self.send_card(title=title,
                       body=description,
                       link=url,
                       fields=(('Status', status), ('Owner', assigned)),
                       color='#f49542',
                       in_reply_to=msg)
        return None


class Jira(JiraHelper):
    @re_botcmd(pattern=r'\bROGUE-[0-9]+\b', prefixed=False, flags=0, hidden=True)
    def rogue(self, msg, args):
        self.send_shit(msg, args.group(0))

    @re_botcmd(pattern=r'\bHUNTER-[0-9]+\b', prefixed=False, flags=0, hidden=True)
    def hunter(self, msg, args):
        self.send_shit(msg, args.group(0))

    @re_botcmd(pattern=r'\bSNOW-[0-9]+\b', prefixed=False, flags=0, hidden=True)
    def snowdrop(self, msg, args):
        self.send_shit(msg, args.group(0))

    @re_botcmd(pattern=r'\bBLUE-[0-9]+\b', prefixed=False, flags=0, hidden=True)
    def blue(self, msg, args):
        self.send_shit(msg, args.group(0))

    _ERR = {
        '_CODES': {
            'C-0': 'RClient_NF_Error: Connection_LostConnectionToGameServer',
            'C-1': 'RClient_NF_Error: Connection_LostConnectionToProxy',
            'C-2': 'RClient_NF_Error: Connection_JoinServerTimedOut',
            'C-3': 'RClient_NF_Error: Grouping_FailedTimeout',
            'C-4': 'RClient_NF_Error: Grouping_FailedRemovedFromGroup',
            'C-5': 'RClient_NF_Error: Grouping_FailedGroupDisbanded',
            'C-6': 'RClient_NF_Error: Grouping_FailedPrivilegesRestriction',
            'C-7': 'RClient_NF_Error: Grouping_FailedGroupFull',
            'C-8': 'RClient_NF_Error: Grouping_FailedPrivacyPrivate',
            'C-9': 'RClient_NF_Error: Grouping_FailedPrivacyOnlyFriends',
            'C-10': 'RClient_NF_Error: Grouping_FailedGroupLeft',
            'C-11': 'RClient_NF_Error: Grouping_CantJoinServerDifferentInstanceType',
            'C-12': 'RClient_NF_Error: Grouping_NotAllowedToGroup',
            'C-13': 'RClient_NF_Error: Privileges_PremiumRestrictedFeature',
            'C-14': 'RClient_NF_Error: Privileges_PremiumRestrictedGroup',
            'C-15': 'RClient_NF_Error: Privileges_SharedEnvironmentsRestricted',
            'C-16': 'RClient_NF_Error: Banned_AccountBanned',
            'C-17': 'RClient_NF_Error: Profile_AuthNetworkError',
            'C-18': 'RClient_NF_Error: Profile_FailedToCreateCharacter',
            'C-19': 'RClient_NF_Error: Profile_FailedToDeleteCharacter',
            'C-20': 'RClient_NF_Error: Profile_FailedToForceUnlockCharacter',
            'C-21': 'RClient_NF_Error: Profile_FailedToRetrieveCharacter',
            'C-22': 'RClient_NF_Error: Profile_FailedToRetrieveCharacterList',
            'C-23': 'RClient_NF_Error: Profile_FailedToUnlockAccount',
            'C-24': 'RClient_NF_Error: Profile_RequestTimedOut',
            'C-25': 'RClient_NF_Error: Profile_AccountLocked',
            'C-26': 'RClient_NF_Error: Profile_UnlockAccount',
            'C-27': 'RClient_NF_Error: Ubiservices_FailedToRenewLoginCredentials',
            'C-28': 'RClient_NF_Error: Ubiservices_UplayDidNotResign',
            'C-29': 'RClient_NF_Error: Ubiservices_UplayDidNotSignup',
            'C-30': 'RClient_NF_Error: Ubiservices_UplayPcCriticalFailure',
            'C-31': 'RClient_NF_Error: Ubiservices_Unreachable',
            'C-32': 'RClient_NF_Error: FirstParty_UserSignout',
            'C-33': 'RClient_NF_Error: PC_UnderMinSpec',
            'C-34': 'RClient_NF_Error: Profile_SurvivalProfile',
            'C-35': 'RClient_NF_Error: Connection_FailedToConnectSurvival',
            'P-0': 'PlatformError: Achievements_FailedToRetrieveAchievements',
            'P-1': 'PlatformError: Connection_LostConnection',
            'P-2': 'PlatformError: FirstPartyDialog_UnknownError',
            'P-3': 'PlatformError: FirstPartyStore_FailToOpenStore',
            'P-4': 'PlatformError: FirstPartyStore_Failed',
            'P-5': 'PlatformError: Friends_FailedToRetrieveBlockList',
            'P-6': 'PlatformError: Friends_FailedToRetrieveMuteList',
            'P-7': 'PlatformError: Friends_FailedToRetrieveFriends',
            'P-8': 'PlatformError: Friends_FailedToRetrieveFriendsName',
            'P-9': 'PlatformError: Friends_FailedToShowProfile',
            'P-10': 'PlatformError: Friends_FailedToRetrievePermissionForFriend',
            'P-11': 'PlatformError: Friends_FailedToRetrieveVoipPermission',
            'P-12': 'PlatformError: Friends_FailedToRetrieveTextPermission',
            'P-13': 'PlatformError: Match_ConnectionFailed',
            'P-14': 'PlatformError: Match_FailedToClearActiveSession',
            'P-15': 'PlatformError: Match_FailedToInviteToSession',
            'P-16': 'PlatformError: Match_FailedToReportEvent',
            'P-17': 'PlatformError: Match_FailedToSetActiveGameSession',
            'P-18': 'PlatformError: Match_FailedToSetSessionHost',
            'P-19': 'PlatformError: Match_GameInvitationRetrieval',
            'P-20': 'PlatformError: Match_GameInviteDeliveredFailed',
            'P-21': 'PlatformError: Match_GameSessionCreation',
            'P-22': 'PlatformError: Match_GameSessionJoin',
            'P-23': 'PlatformError: Match_GameSessionLeave',
            'P-24': 'PlatformError: Match_GameSessionRetrieval',
            'P-25': 'PlatformError: Match_GameSessionRetrievalNoRetry',
            'P-26': 'PlatformError: Match_GameSessionSearch',
            'P-27': 'PlatformError: Match_GameSessionUpdate',
            'P-28': 'PlatformError: Match_NoSessionWithName',
            'P-29': 'PlatformError: Match_ProtocolActivationError',
            'P-30': 'PlatformError: Match_SessionFailure',
            'P-31': 'PlatformError: Match_UnknownError',
            'P-32': 'PlatformError: Party_GetMembersFailed',
            'P-33': 'PlatformError: Party_GetMembersInfo',
            'P-34': 'PlatformError: Party_GetMembersNameFailed',
            'P-35': 'PlatformError: Party_Initialize',
            'P-36': 'PlatformError: Party_LoadModule',
            'P-37': 'PlatformError: Party_RegisterCallback',
            'P-38': 'PlatformError: Presence_FailedToRetrievePresence',
            'P-39': 'PlatformError: Presence_FailedToSetPresence',
            'P-40': 'PlatformError: Privileges_FailedAgeRating',
            'P-41': 'PlatformError: Privileges_FailedToRetrievePrivileges',
            'P-42': 'PlatformError: Privileges_MissingLatestSystemSoftware',
            'P-43': 'PlatformError: Privileges_MissingPatch',
            'P-44': 'PlatformError: Privileges_UnknownAvailability',
            'P-45': 'PlatformError: Privileges_PatchCheckFailed',
            'P-46': 'PlatformError: Privileges_UnknownError',
            'P-47': 'PlatformError: RealTimeData_ConnectionFailed',
            'P-48': 'PlatformError: RealTimeData_FailedToRetrievePresence',
            'P-49': 'PlatformError: RealTimeData_RemovePresenceSubscriptionFailed',
            'P-50': 'PlatformError: Reputation_FailedGetReputationRequest',
            'P-51': 'PlatformError: Reputation_FailedSetReputationRequest',
            'P-52': 'PlatformError: Recording_FailedToInitialise',
            'P-53': 'PlatformError: Recording_FailedToSetScreenResource',
            'P-54': 'PlatformError: Recording_FailedToSetState',
            'P-55': 'PlatformError: Recording_FailedToTerminate',
            'P-56': 'PlatformError: SharePlay_FailedToSetState',
            'P-57': 'PlatformError: Statistics_FailedToSendStatisticUpdate',
            'P-58': 'PlatformError: Statistics_StatisticRequestFailed',
            'P-59': 'PlatformError: Ubiservices_LoginFailed',
            'P-60': 'PlatformError: Ubiservices_LogoutFailed',
            'P-61': 'PlatformError: Ubiservices_NewsRetrieveFailed',
            'P-62': 'PlatformError: Ubiservices_UplaySmartFailedToOpen',
            'P-63': 'PlatformError: Ubiservices_UplayWinActionRetrieveFailed',
            'P-64': 'PlatformError: Ubiservices_UplayWinActionUnlockFailed',
            'P-65': 'PlatformError: Ubiservices_UplayWinRewardPurchaseFailed',
            'P-66': 'PlatformError: Ubiservices_UplayWinRewardRetrieveFailed',
            'P-67': 'PlatformError: Ubiservices_ProfileFailedToGet',
            'P-68': 'PlatformError: User_FailedToRegisterForEvents',
            'P-69': 'PlatformError: User_GamerPicRequestFailed',
            'P-70': 'PlatformError: User_LoggedOut',
            'P-71': 'PlatformError: User_MissingFirstPartyAccount',
            'P-72': 'PlatformError: User_NoUplayPCConnection',
            'P-73': 'PlatformError: User_LoggedOutUplayPC',
            'P-74': 'PlatformError: User_NoUserFound',
            'P-75': 'PlatformError: WebBrowser_FailedToOpen',
            },
    '_CLASS': {
            'ALPHA': {'contact': 'Tim Kirk, Richard Allen', 'category': 'Category: AccountPicker'},
            'BRAVO': {'contact': 'Tim Kirk, Richard Allen', 'category': 'Category: Achievements'},
            'CHARLIE': {'contact': 'Tim Kirk, Richard Allen', 'category': 'Category: ActivityFeed'},
            'DELTA': {'contact': 'Bjorn Lindberg, Rasmus Neckelmann', 'category': 'Category: Connection'},
            'ECHO': {'contact:': 'Tim Kirk, Richard Allen', 'category': 'Category: FirstPartyDialog'},
            'FOXTROT': {'contact:': 'Julien Reiss', 'category': 'Category: FirstPartyStore'},
            'GOLF': {'contact:': 'Ahmad Mouhsen', 'category': 'Category: Friends'},
            'HOTEL': {'contact:': 'Ahmad Mouhsen', 'category': 'Category: Grouping'},
            'INDIA': {'contact:': 'Ahmad Mouhsen', 'category': 'Category: Match'},
            'JULIET': {'contact:': 'Ahmad Mouhsen', 'category': 'Category: Party'},
            'KILO': {'contact:': 'Tim Kirk, Richard Allen', 'category': 'Category: Presence'},
            'LIMA': {'contact:': 'Tim Kirk, Richard Allen', 'category': 'Category: Privileges'},
            'MIKE': {'contact:': 'Bjorn Lindberg, Rasmus Neckelmann', 'category': 'Category: Profile'},
            'NOVEMBER': {'contact:': 'Tim Kirk, Richard Allen', 'category': 'Category: RealTimeData'},
            'OSCAR': {'contact:': 'Tim Kirk, Richard Allen', 'category': 'Category: Recording  '},
            'PAPA': {'contact:': 'Tim Kirk, Richard Allen', 'category': 'Category: Reputation'},
            'QUEBEC': {'contact:': 'Tim Kirk, Richard Allen', 'category': 'Category: Statistics'},
            'ROMEO': {'contact:': 'Tim Kirk, Richard Allen', 'category': 'Category: Ubiservices'},
            'SIERRA': {'contact:': 'Tim Kirk, Richard Allen', 'category': 'Category: User'},
            'TANGO': {'contact:': 'Tim Kirk, Richard Allen', 'category': 'Category: WebBrowser'},
            'UNIFORM': {'contact:': 'Rasmus Neckelmann', 'category': 'Category: Survival'},
            'VICTOR': {'contact:': 'Tim Kirk, Richard Allen', 'category': 'Category: ContentPatch'}
            }
    }

    @re_botcmd(pattern=r'\b(ALPHA|BRAVO|CHARLIE|DELTA|ECHO|FOXTROT|GOLF|HOTEL|INDIA|JULIET|KILO|LIMA|MIKE|NOVEMBER|OSCAR|PAPA|QUEBEC|ROMEO|SIERRA|TANGO|UNIFORM|VICTOR) [CP]-[0-9][0-9]?\b', prefixed=False, flags=0, hidden=True)
    def delta_err(self, msg, args) -> str:
        _error_msg = args.group(0)
        _error_class = _error_msg.split(' ')[0]
        _error_code = _error_msg.split(' ')[1]
        return f"`{_error_msg}` means `{self._ERR['_CLASS'][_error_class]['category']}` `{self._ERR['_CODES'][_error_code]}`"
