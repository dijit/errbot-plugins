#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Don't ask
"""
import logging
import requests
import datetime
import time

if __name__ == "__main__":
    import r_secrets
else:
    from . import r_secrets

# import yaml ## FIXME: not in requirements.txt, unused
"""
curl -H "Authorization: Bearer <token>" http://msr-tctd-pgrafana01/api/dashboards/home
"""

_token = f'Bearer {r_secrets.secrets["grafana"]}'
LOGGER = logging.getLogger(__name__)
LOGGER.setLevel(logging.DEBUG)
grafana_host = "grafana.prod.tctd2.ubisoft.com"


def big_screen_dash_test(_from=1513604050864, _to=1513690450864):
    _tz = "CEST"
    LOGGER.debug(f"Range set from {_from} to {_to}")
    _url = (
        f"http://{grafana_host}/render/dashboard-solo/db/big-screen-dash-2?orgId=1&panelId=1&from={_from}&to={_to}&width=1000&height=800&tz={_tz}&kiosk=1&theme=light"
    )
    headers = {"Authorization": _token}
    _resp = requests.get(_url, headers=headers, stream=True)
    if _resp.ok:
        return _resp.raw
    else:
        raise Exception


def solo_get(_from=0, _to=0, dashboardname="big-screen-dash-2", panelid=1):
    _tz = "CEST"
    LOGGER.debug(f"Range set from {_from} to {_to}")
    _url = (
        f"http://{grafana_host}/render/dashboard-solo/db/{dashboardname}?orgId=1&panelId={panelid}&from={_from}&to={_to}&width=1000&height=800&tz={_tz}&kiosk=1&theme=light"
    )
    headers = {"Authorization": _token}
    _resp = requests.get(_url, headers=headers, stream=True)
    if _resp.ok:
        return _resp.raw
    else:
        raise Exception


def dashboard_get(_from=0, _to=0, dashboardname="big-screen-dash-2"):
    _tz = "CEST"
    LOGGER.debug(f"Range set from {_from} to {_to}")
    _url = (
        f"http://{grafana_host}/render/dashboard/db/{dashboardname}?orgId=1&from={_from}&to={_to}&width=1400&height=800&tz={_tz}&kiosk=1&theme=light"
    )
    headers = {"Authorization": _token}
    _resp = requests.get(_url, headers=headers, stream=True)
    if _resp.ok:
        return _resp.raw
    else:
        raise Exception


if __name__ == "__console__" or __name__ == "__main__":
    ch = logging.StreamHandler(sys.stdout)
    ch.setLevel(logging.DEBUG)
    formatter = logging.Formatter(
        "%(asctime)s - %(name)s - %(levelname)s - %(message)s"
    )
    ch.setFormatter(formatter)
    LOGGER.addHandler(ch)
