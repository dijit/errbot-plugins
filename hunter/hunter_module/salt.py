#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Don't ask
"""
import logging
import json
import requests
import time
import sys

if __name__ == "__main__":
    import r_secrets
else:
    from . import r_secrets

# import yaml ## FIXME: not in requirements.txt, unused
"""
What are we trying to emulate here:

~ » curl -sS http://msr-tctd-csalt01/login
    -H 'Accept: application/json' -d username=jharasym
    -d password=$(~/bin/password2.py -o) -d eauth=pam
Password:
{'return': [{'eauth': 'pam',
             'expire': 1511491622.020057,
             'perms': {},
             'start': 1511448422.020051,
             'token': '0fc055317092d27db19e9df676a3bd8ef1ee9ab7',
             'user': 'jharasym'}]}

~ » curl -sS http://msr-tctd-csalt01 -H 'Content-Type: application/json'
    -H "X-Auth-Token: 963fdd2cb276b73a64ebd87aaa15d0e4da812e35"
    -d '[{"client":"local","tgt":"msr-tctd-s*cs01","fun":"test.ping"}]'
{'return': [{'msr-tctd-s1cs01': True,
             'msr-tctd-s4cs01': True,
             'msr-tctd-spcs01': True}]}
"""

LOGGER = logging.getLogger(__name__)
LOGGER.setLevel(logging.DEBUG)

if __name__ == "__console__" or __name__ == "__main__":
    ch = logging.StreamHandler(sys.stdout)
    ch.setLevel(logging.DEBUG)
    formatter = logging.Formatter(
        "%(asctime)s - %(name)s - %(levelname)s - %(message)s"
    )
    ch.setFormatter(formatter)
    LOGGER.addHandler(ch)

_SALT_MASTER_MAP = {
    "prod": {"server": "https://salt-api.prod.tctd2.ubisoft.com", "token": "", "expire": 0.0},
    "cert": {"server": "https://salt-api.cert.tctd2.ubisoft.com", "token": "", "expire": 0.0},
    "thor": {"server": "https://salt-api.thor.tctd2.ubisoft.com", "token": "", "expire": 0.0},
    "night": {"server": "https://salt-api.night.tctd2.ubisoft.com", "token": "", "expire": 0.0},
    "loki": {"server": "https://salt-api.loki.tctd2.ubisoft.com", "token": "", "expire": 0.0},
    "freyja": {"server": "https://salt-api.freyja.tctd2.ubisoft.com", "token": "", "expire": 0.0},
}  # _SALT_MASTER_MAP

"""
########################## FOR MAINTENANCE ####################################
"""


def state_apply_core() -> bool:
    """
    default to prod, state apply all core for rogue
    """
    salt_cmd(
        environment="prod",
        target="G@os:Windows and E@msr-tctd-p[4,1,p]cs0[1,2]",
        action="state.sls",
        tgt_type="compound",
        arg="game.core",
    )
    return True


def state_apply_proxies() -> bool:
    """
    default to prod, state apply all proxies for rogue
    """
    # salt -b 30 -C 'G@os:Windows and E@.*-p[1,4,p]p[0-9][0-9][0-9]' state.sls game.game_proxy --state_output=mixed
    # sleep 1
    # salt -b 20 -C 'G@os:Windows and E@msr-tctd-p[1,4,p]cp.*' state.sls game.core_proxy --state_output=mixed
    # _ret_gp = salt_cmd(
    salt_cmd(
        environment="prod",
        target="G@os:Windows and E@.*-p[1,4,p]p[0-9][0-9][0-9]",
        action="state.apply",
        tgt_type="compound",
        arg="game.game_proxy",
    )
    # _ret_cp = salt_cmd(
    salt_cmd(
        environment="prod",
        target="G@os:Windows and E@msr-tctd-p[1,4,p]cp.*",
        action="state.apply",
        tgt_type="compound",
        arg="game.core_proxy",
    )
    # _ret = {}
    # _ret.update(_ret_gp)
    # _ret.update(_ret_cp) ## Think about checking the json validator output.
    return True


"""
########################## END MAINTENANCE ####################################
"""

def download_build(env: str, build_cl=None) -> str:
    if env not in _SALT_MASTER_MAP.keys():
        return 'Failed, I do not know the environment: ' + environment
    if build_cl is None:
        return 'Failed, I need to know what the CODECL is.'
    _ret_dl = salt_cmd(environment=env,
             target="G@role:p2p or G@role:gameserver or G@role:core or G@role:proxy",
             action="hunter.download_build",
             tgt_type="compound",
             arg=str(build_cl)
             )
    log.debug(_ret_dl)
    _ret_pOK = salt_cmd(environment=env,
             target="G@role:p2p or G@role:gameserver or G@role:core or G@role:proxy",
             action="hunter.patch_ok",
             tgt_type="compound",
             arg=str(build_cl)
             )
    log.debug(_ret_pOK)
    _ret_stop = salt_cmd(environment=env,
             target="G@role:p2p or G@role:gameserver or G@role:core or G@role:proxy",
             action="hunter.stop_all",
             tgt_type="compound",
             arg=""
             )
    log.debug(_ret_stop)
    _ret_reset = salt_cmd(environment=env,
             target="G@role:p2p or G@role:gameserver or G@role:core or G@role:proxy",
             action="service.restart",
             tgt_type="compound",
             arg="HunterService"
             )
    log.debug(_ret_reset)
    return "#YOLO"

def pretty_salt_cmd(environment, target, action, tgt_type="glob", arg=None):
    if arg:
        json_res = salt_cmd(environment, target, action, arg)
    else:
        json_res = salt_cmd(environment, target, action)
    useful_shit = {}
    keys = []
    for host in json_res.keys():
        useful_shit.update({host: {}})
        for x in json_res[host].keys():
            try:
                useful_shit.update({json_res[host][x]: json_res[host][x]["name"]})
            except KeyError as e:
                LOGGER.error(
                    "Failed to add shit to the output: " + json.dumps(json_res[x])
                )

    return keys


def salt_cmd(environment, target, action, tgt_type="glob", arg=None):

    """
    Send a request to salt-api
    """
    if not token_get(environment):
        return "I am unable to get a new API token and I need one to carry this out."
    url = _SALT_MASTER_MAP[environment]["server"]
    headers = {"X-Auth-Token": _SALT_MASTER_MAP[environment]["token"]}
    target = target.strip("'").strip('"')
    data = {
        "client": "local",
        "tgt": target,
        "tgt_type": tgt_type,
        "full_return": False,
        "fun": action,
    }
    if arg:
        data["arg"] = arg
    LOGGER.debug("Data is " + repr(data))
    LOGGER.debug("Running salt-api request {} towards {}".format(action, target))
    LOGGER.debug("Outbound request: {} {} {}".format(url, str(data), str(headers)))
    res = requests.post(url, json=data, headers=headers, verify=False)
    LOGGER.debug("Response: " + res.text)
    if not res.ok:
        LOGGER.error("I am not authorized to talk to salt: " + str(res.status_code))
    LOGGER.debug(f"Response: {res.json()}")
    return res.json().get("return")[0]


def token_get(environment):
    """
    :params: String which corresponds to _SALT_MASTER_MAPs top level
    :returns: True if good, False if you don't have a good token.
    :info: this function checks the expiry of the tokens in the cache
            and updates the cache if it has expired.
    """
    if environment not in _SALT_MASTER_MAP.keys():
        raise Exception("Fail, Environment not known")
    try:
        if token_expired(environment):
            LOGGER.debug("Requesting new token")
            url = _SALT_MASTER_MAP[environment]["server"] + "/login"
            headers = {"Accept": "application/json"}
            _user = 'salt-api'
            _passwd = 'sirdicks'
            #passwd = get_password()
            data = {"username": _user, "password": _passwd, "eauth": "pam"}
            LOGGER.debug("sending: {} to {}".format(str(data), url))
            res = requests.post(url, data=data, headers=headers, verify=False)
            LOGGER.debug("Response when retrieving token: " + str(res.status_code))
            if res.status_code != 200:
                LOGGER.error("Retreival of access token failed with: " + res.text)
                return False
            token_data = res.json()
            _SALT_MASTER_MAP[environment]["token"] = token_data["return"][0]["token"]
            _SALT_MASTER_MAP[environment]["expire"] = token_data["return"][0]["expire"]
            pretty_expiry = _pretty_time(_SALT_MASTER_MAP[environment]["expire"])
            LOGGER.debug(f"Updated token, new expiry {pretty_expiry}")
            return True
        pretty_expires = _pretty_time(_SALT_MASTER_MAP[environment]["expire"])
        LOGGER.debug(f"Reusing token, expires {pretty_expires}")
        return True
    except Exception as e:
        LOGGER.debug(f"Failed to update token {e}")
        return False


"""
############################## TOKEN MANAGEMENT ################################
"""


def token_expire(environment: str) -> bool:
    if environment not in _SALT_MASTER_MAP.keys():
        return False
    timenow = time.time()
    _SALT_MASTER_MAP[environment]["expire"] = timenow
    return True


def token_expired(environment: str) -> bool:
    """
    returns True if the token in the cache has expired
    """
    expiry = _SALT_MASTER_MAP[environment]["expire"]
    expiry_pretty = time.strftime("%Y-%m-%d %H:%M:%S", time.localtime(expiry))
    if expiry > time.time():
        LOGGER.debug(f"Token for {environment} is not expired, expires {expiry_pretty}")
        return False
    LOGGER.debug(f"Token for {environment} has expired, expired {expiry_pretty}")
    return True


def token_return(environment: str) -> str:
    """
    for debugging, print the current token and expiry for a given key.
    :param environment: prod, staging, cert (as map above)
    :return: stringy thing with expiry and current token
    """
    try:
        return (
            f"{_SALT_MASTER_MAP[environment]['expire']} I'm not giving you the key though."
        )
    except IndexError:
        return (
            f"Please supply a valid environment: {', '.join(_SALT_MASTER_MAP.keys())}"
        )


"""
############################## TOKEN MANAGEMENT ################################
"""


def _return_failed(salt_return: dict):
    """
    Filter out all the failed jobs
    :param salt_return: dict which is given by salt.
    :return: list of failed hosts or boolean true
    """
    pass


def _pretty_time(timecode):
    return time.strftime("%Y-%m-%d %H:%M:%S", time.localtime(timecode))


"""
I use this to get an API token in the bot
"""


def get_password():
    return r_secrets.return_windows_password()


def _find_patch(role: str = None, platform: str = "prod", state: int = 1) -> dict:
    """
    Good to scan for patches in a transitional state
    good states to check are:
        - 1: bad
        - 2: creating 
        - 4: downloading
        - 6: verifying
        - 9: extracting
        - 11: removing
    EG:
        _ret = _find_patch()
        if _ret:
            print(repr(_ret))
        else:
            print("No bad patches found")
    """

    def _find_patch_role(role: str, platform: str, state: int) -> dict:
        _inner_patches = {}
        _ret = salt_cmd(
            f"{platform}", f"role:{role}", "rogue.patch_info", tgt_type="grain"
        )
        LOGGER.error(repr(_ret))
        for host in _ret.keys():
            try:
                for version in _ret[host]["stats"].get("versions"):
                    if version["state"] == state:
                        _inner_patches.update(
                            {
                                f"{host} http://msv-rog-web01/web/app/auto_restarter/auto_restarter.html?host={host}.ubisoft.onbe:14020": version[
                                    "build"
                                ]
                            }
                        )
            except TypeError as e:
                LOGGER.error(f"{host} has issue " + repr(_ret[host]))
                pass
        return _inner_patches

    if role is None:
        outer_patches = {}
        LOGGER.debug("Role is not specified, trying all")
        for _role in "gameserver", "pps", "core", "proxy":
            LOGGER.debug(f"Trying: {_role}")
            outer_patches.update(_find_patch_role(_role, platform, state))
        return outer_patches
    else:
        LOGGER.debug(f"Trying: {role}")
        return _find_patch_role(role, platform, state)


def find_verified_patches(state: int = 7) -> str:
    """
    Implements "_find_patch" with more friendly words.
    """
    _ret = _find_patch(None, "prod", state)
    if _ret:
        return "Found: " + ", ".join(
            "{} : {}".format(key, value) for key, value in _ret.items()
        )
    else:
        return "Did not find any patches matching your description."


def find_patch_states(state: int = 1) -> str:
    """
    Implements "_find_patch" with more friendly words.
    """
    known_states = {
        1: "bad",
        2: "creating",
        4: "downloading",
        6: "verifying",
        9: "extracting",
        11: "removing",
    }
    if state not in known_states.keys():
        return "That state is not a transition"

    _ret = _find_patch(None, "prod", state)
    if _ret:
        return "Found: " + ", ".join(
            "{} : {}".format(key, value) for key, value in _ret.items()
        )
    else:
        return f"Did not find any patches that were {known_states[state]}."


if __name__ == "__main__":
    from pprint import pprint

    pprint(salt_cmd("hunt", "ew1-web01", "test.ping"))
