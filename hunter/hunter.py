from errbot import BotPlugin, botcmd, re_botcmd
import re
import logging
if __name__ == '__main__':
    from .hunter_module import salt, hacky_uploader, error_codes, grafana
else:
    from hunter_module import salt, hacky_uploader, error_codes, grafana

logger = logging.getLogger(__name__)


class Hunter(BotPlugin):
    @botcmd(split_args_with=None)
    def hunter_patch_download(self, msg, args):
        help_string = r"""Unable to comply, I couldn't parse your request:
        !hunter patch download <env> <cl>
        ex: !hunter patch download prod 114500
        or: !hunter patch download cert <CL from CODECL.txt>
        """
        if len(args) != 2:
            logger.debug("Help string is requested due to low args: " + repr(args))
            return help_string
        return salt.download_build(args[0], args[1])

    @botcmd(split_args_with=None)
    def hunter_salt_check(self, msg, args):
        if len(args) != 1:
            return "Env?"
        if salt.token_get(args[0]):
            return "Token got"
        else:
            return "Something failed"

    @botcmd(split_args_with=None)
    def hunter_patch_upload(self, msg, args):
        help_string = r"""Unable to comply, I couldn't parse your request:
        !hunter patch upload <env> <build> 
        ex: !hunter patch upload prod s004404.RC.cl1527143.default
        or: !hunter patch upload cert <Build Name as it appears on the share>
        """
        if len(args) != 2:
            logger.debug("Help string is requested due to low args: " + repr(args))
            return help_string
        return hacky_uploader.hacky_uploader(args[0], args[1])

    @re_botcmd(pattern=r'(ALFA-?[0-9]?[0-9]|BRAVO[0-9][0-9]|CHARLIE[0-9][0-9]|DELTA[0-9][0-9]|ECHO[0-9][0-9]|FOXTROT[0-9][0-9]|MIKE[0-9][0-9]|OSCAR[0-9][0-9]|UNIFORM[0-9][0-9])', prefixed=False, flags=re.IGNORECASE, hidden=True)
    def fe_code(self, msg, args):
        if error_codes.get_fe_error(args.group(1)):
            return error_codes.get_fe_error(args.group(1))


    @botcmd(split_args_with=None)
    def capacity(self, msg, args):
        if len(args) < 2:
            _from = (
                datetime.datetime.now(datetime.timezone.utc)
                - datetime.timedelta(minutes=20)
            ).timestamp() * 1e3
            _to = datetime.datetime.now(datetime.timezone.utc).timestamp() * 1e3
        yield "Please stand by, data acquisition in progress."
        self.send_stream_request(
            msg.frm,
            grafana.dashboard_get(int(_from), int(_to), "dc-capacity-numeric-new"),
            name="Cap " + datetime.datetime.now().isoformat(),
        )

    @botcmd(split_args_with=None)
    def ccu(self, msg, args):
        if len(args) < 2:
            _from = (
                datetime.datetime.now(datetime.timezone.utc)
                - datetime.timedelta(hours=3)
            ).timestamp() * 1e3
            _to = datetime.datetime.now(datetime.timezone.utc).timestamp() * 1e3
        yield "Please stand by, data acquisition in progress."
        try:
            self.send_stream_request(
                msg.frm,
                grafana.big_screen_dash_test(_from, _to),
                name="CCU " + datetime.datetime.now().isoformat(),
            )
            self.send_stream_request(
                msg.frm,
                grafana.solo_get(_from, _to, "brooklyn-vs-manhattan", 10),
                name="Total CCU " + datetime.datetime.now().isoformat(),
            )
        except Exception as e:
            yield "I didn't manage to upload the graphs like you wanted. " + e


    @botcmd(split_args_with=None)
    def hunter_running_build(self, msg, args):
        help_string = r"""!hunter running build <prod|cert|thor>
        Get info on what CL is currently live on prod, and what's waiting (if anything)"""
        if len(args) != 1:
            return help_string
        if salt.token_expired(args[0]):
            yield f"Hi {msg.frm}, please stand-by while I check {args[0]}"
        try:
            _test_host = 'gcp-net-g7001'
            patch_info = salt.salt_cmd(args[0], _test_host, 'hunter.patch_info').get(_test_host, {})
            _latest_enabled = 0
            _latest_downloaded = 0
            for patch in sorted(patch_info):
                if patch_info[patch] == 'ENABLED':
                    _latest_enabled = int(patch)
                elif patch_info[patch] == 'EXTRACTED':
                    _latest_downloaded = int(patch)
            if _latest_enabled < _latest_downloaded:
                yield "I see `{0}` currently running, but `{1}` is waiting to be enabled".format(
                    _latest_enabled, _latest_downloaded
                )
            else:
                yield f"CL `{_latest_enabled}` is currently running"
        except:
            yield "I am sorry, I have failed in my attempts to reach the environment, please ask @jmh to fix me"
