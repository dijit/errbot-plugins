from errbot import BotPlugin, webhook
"""
for the td2_launch
"""
from os import chdir
import subprocess
"""
/for the td2_launch
"""
import logging

logger = logging.getLogger(__name__)


class alerting(BotPlugin):
    @webhook('/alert/')
    def msg_channel(self, payload):
        logger.debug(repr(payload))
        if payload['state'] == 'ok':
            status = ":white_check_mark: Issue resolved"
        elif payload['state'] == 'alerting':
            status = ":warning: Warning"
        self.send(
                self.build_identifier('#gns'),
                f'{status}, {payload["message"]}'
                )
        return None
    """
    @webhook
    def test(self):
        logger.debug("Got something!!!!!!")
        return None
    """
    @webhook('/msg_chan/<channel>/')
    def talkbotfunc(self, request, channel):
        logger.debug(f"{repr(request)}")
        logger.debug(f"sending {request['message']} to {channel}")
        self.send(self.build_identifier(str(f"#{channel}")), request['message'])
        return None

    @webhook('/maintenance/<channel>/')
    def maintfunc(self, request, channel):
        logger.debug(f"{request}")
        logger.debug(f"maintenance message incoming: {request['body']} to {channel}")
        channel = self.build_identifier(str(f"#{channel}"))
        self.send_card(title="Maintenance has been mentioned on @TheDivisionGame",
                       thumbnail=request.get("image", None),
                       link=request.get("link", None),
                       body=request.get("body", None),
                       color="red",
                       to=channel)
        return None

    @webhook('/td2_launch/')
    def launcfunc(self, request):
        logger.debug(f"{request}")
        # list of channel ID's:
        # GNS : CEV1FF4EB
        # i3d : C2H1KRP5L
        # launch : CG1ULEQPQ
        # infra_launch : CEHG6DNNS
        chdir('/Users/jharasym/projects/msv/hunter-terraform/environments/prod')
        cmd_list = ['git', 'pull']
        subprocess.check_output(cmd_list)
        cmd_list = ['terraform', 'apply', '-auto-approve', '-no-color', '-target=module.network']
        subprocess.check_output(cmd_list)
        for x in ['CEV1FF4EB', 'C2H1KRP5L', 'CG1ULEQPQ', 'CEHG6DNNS']:
            channel = self.build_identifier(str(f"<#{x}>"))
            self.send_card(title="The Division 2 Launch button has been pressed",
                           thumbnail="https://i.imgur.com/R775f7m.jpg",
                           # link='https://en.wikipedia.org/wiki/Tom_Clancy%27s_The_Division_2',
                           body="Welcome to running a live-game. May the servers hold and the metacritic score be high.",
                           color="green",
                           to=channel)
        return None

    @webhook('/fake_td2_launch/')
    def fakelauncfunc(self, request):
        logger.debug(f"{request}")
        # list of channel ID's:
        # GNS : CEV1FF4EB
        # i3d : C2H1KRP5L
        # launch : CG1ULEQPQ
        # infra_launch : CEHG6DNNS
        chdir('/Users/jharasym/projects/msv/hunter-terraform/environments/prod')
        cmd_list = ['git', 'pull']
        subprocess.check_output(cmd_list)
        cmd_list = ['terraform', 'apply', '-auto-approve', '-no-color', '-target=module.network']
        subprocess.check_output(cmd_list)
        for x in ['C8THM90JG']:
            channel = self.build_identifier(str(f"<#{x}>"))
            self.send_card(title="The Division 2 Launch button has been pressed",
                           thumbnail="https://i.imgur.com/R775f7m.jpg",
                           # link='https://en.wikipedia.org/wiki/Tom_Clancy%27s_The_Division_2',
                           body="Welcome to running a live-game. May the servers hold and the metacritic score be high.",
                           color="green",
                           to=channel)
        return None
