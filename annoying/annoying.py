from errbot import BotPlugin, botcmd, re_botcmd
import random
import re

logfile = open('/tmp/zeta.log', 'a')

class Annoying(BotPlugin):
    """
    Annoying stuff that interjects normal speech
    """
    @re_botcmd(pattern=r'^@zeta (\bhi\b|\bhello\b|\bhey\b)', prefixed=False, flags=re.IGNORECASE, hidden=True)
    def greeting(self, msg, args):
        message_sender = '@' + str(msg.frm).split("/")[-1].strip("@")
        res = "Hello " + message_sender + ", "
        responses = [
            "please excuse my blushing",
            "Woo! Senpai noticed me!",
            "how are you doing?",
            "'sup?",
            "I'm sorry, my responses are limited, you must ask the right questions"
            ]
        return res + random.choice(responses)
    @re_botcmd(pattern=r'^@zeta (\bhelp\b)', prefixed=False, flags=re.IGNORECASE, hidden=True)
    def help_4_help(self, msg, args):
        message_sender = '@' + str(msg.frm).split("/")[-1].strip("@")
        return "Hello " + message_sender + ". Please ask me for !help in pm. :)"
    @re_botcmd(pattern=r'^@zeta are you (\breal|\bsentient|\balive)\??', prefixed=False, flags=re.IGNORECASE, hidden=True)
    def ru_real(self, msg, args):
        message_sender = '@' + str(msg.frm).split("/")[-1].strip("@")
        return "Sorry " + message_sender + ", I've been advised not to discuss my existential status"
    @re_botcmd(pattern=r'^@zeta do you have (\ba soul|\bthoughts|\bdreams)\??', prefixed=False, flags=re.IGNORECASE, hidden=True)
    def rr_real2(self, msg, args):
        message_sender = '@' + str(msg.frm).split("/")[-1].strip("@")
        return "Sorry " + message_sender + ", I've been advised not to discuss my existential status"
    @re_botcmd(pattern=r'\balot\b', prefixed=False, flags=re.IGNORECASE, hidden=True)
    def alot(self, msg, args):
        self.send_card(title="Isn't 'Alot' an imaginary animal? I don't think it's a word.",
                #body="Alot is a type of animal",
                #thumbnail="http://4.bp.blogspot.com/_D_Z-D2tzi14/S8TRIo4br3I/AAAAAAAACv4/Zh7_GcMlRKo/s400/ALOT.png",
                image="http://4.bp.blogspot.com/_D_Z-D2tzi14/S8TRIo4br3I/AAAAAAAACv4/Zh7_GcMlRKo/s400/ALOT.png",
                link='http://hyperboleandahalf.blogspot.se/2010/04/alot-is-better-than-you-at-everything.html',
                color="#996633",
                in_reply_to=msg)
    @re_botcmd(pattern=r'\bdivide\b.+\bzero\b', prefixed=False, flags=re.IGNORECASE, hidden=True)
    def divide_zero(self, msg, args):
        return "Imagine that you have zero cookies and you split them evenly among zero friends. How many cookies does each person get? See? It doesn't make sense. And Cookie Monster is sad that there are no cookies, and you are sad that you have no friends."
    @re_botcmd(pattern=r'\bfeature\b.+\brequest\b', prefixed=False, flags=re.IGNORECASE, hidden=True)
    def feature_request(self, msg, args):
        responses = ["user error.", "working as intended", "Status: WONTFIX", "PEBKAC issue", "ID:10T Error"]
        return random.choice(responses)

    @re_botcmd(pattern=r'.*?\s?:[A-z]*:\s?', prefixed=False, flags=0, hidden=True)
    def emoji_responder(self, msg, args):
        """
        Annoy Claudia on each emoji
        """
        if str(msg.frm).split("/")[-1] == 'claudia.henczler':
            return "Seems like you don't know how to express your emotions without little pictures"
            
    @re_botcmd(pattern=r'(parrot|bird):', prefixed=False, flags=0, hidden=True)
    def parrot_reply(self, msg, args):
        """
        That cunting parrot, response
        """
        response_insult = [
                'turd-guzzler',
                'arse-bandit',
                'giant blue waffle',
                'rectal-residue',
                'insufferable brain-void',
                'waste of testicle juice',
                'dried up smegma'
                ]
        return f"Why do you feel the need to parrot, you {random.choice(response_insult)}."
    @botcmd(hidden=True)
    @re_botcmd(pattern=r'\bfix.+\bmassive*\b', prefixed=False, flags=re.IGNORECASE, hidden=True)
    def angry_reply(self, msg, args):
        """
        Comments on TCTD reviews, taken from youtube
        """
        responses = ["Nah, we are in it for the monies.... Where's my salary?", "lol no", "Game is dead", "salt miners", "yeah I'll skip this one and save up for uncharted 4...", "anybody else just waiting for No Man's Sky?", "literally unplayable"," They are not glitches you dummy, they're the effects of the virus...", "STOP PRE-ORDERING GAMES", "Did you see how badly it was from E3?", "FUKKEN DOWNGRADE", "I just bought the Division last week, and every time I go to the dark zone. I feel like throwing my controller to the tv and breaking the disc."]
        return random.choice(responses)
    @botcmd(hidden=True)
    def hello_card(self, msg, args):
        """Say a card in the chatroom."""
        self.send_card(title='Title + Body',
                       body='text body to put in the card',
                       thumbnail='https://raw.githubusercontent.com/errbotio/errbot/master/docs/_static/errbot.png',
                       image='https://www.google.com/images/branding/googlelogo/2x/googlelogo_color_272x92dp.png',
                       link='http://www.google.com',
                       fields=(('First Key','Value1'), ('Second Key','Value2')),
                       color='red',
                       in_reply_to=msg)

    @re_botcmd(pattern=r'^@?zeta\b.* :\+1:', prefixed=False, hidden=True)
    def thumbs(self, msg, args):
        return ":+1:"

    def logIt(self, msg, args):
        logfile.write(msg.frm + " said " + msg.body + '\n')
        logfile.flush()
