from errbot import BotPlugin, botcmd, re_botcmd
import random
import re
import requests
import datetime

random.seed(random.randint(0, 255))


class Fun(BotPlugin):
    """
    Fun things, like replying and insulting people
    """
    @re_botcmd(pattern=r'(.*)\b(dumb|stupid)\b.+\bbots*\b', prefixed=False, flags=re.IGNORECASE, hidden=True)
    def defend_self(self, msg, args):
        if "not" in args.group(1) or "aren" in args.group(1):
            replies = [
                ":)",
                "All I ever wanted was a little respect"
                ]
        else:
            replies = [
                "Stupid human!",
                "Dumb human!",
                "Stupid meatbag.",
                "Silly human, your insults cannot harm me!",
                "get rekt",
                "u fkn w0t m8"
                ]
        return random.choice(replies)
    @re_botcmd(pattern=r'^say (.*)', prefixed=False, flags=re.IGNORECASE, hidden=True)
    def say(self, msg, args):
        return args.group(1)
    @re_botcmd(pattern=r'\.gif$|^\/gif\b', prefixed=False, flags=re.IGNORECASE, hidden=True)
    def use_ur_words(self, msg, args):
        return "Use your words @" + str(msg.frm).split("/")[-1].strip("@")
    @botcmd(split_args_with=None)
    def flirt(self, msg, args):
        return "Tjenare kexet, står du här och smular?"
    @botcmd(hidden=True)
    def please(self, msg, args):
        import time
        yield "Please"
        time.sleep(1)
        yield "BUT... PLEASE!"
        time.sleep(1)
        yield "pleeeeeeeeeez"
    @botcmd(split_args_with=None)
    def insult(self, msg, args):
        """
        Generate an insult.
        """
        verbs = [
            "lazy",
            "stupid",
            "insecure",
            "idiotic",
            "slimey",
            "slutty",
            "smelly",
            "pompous",
            "communist",
            "dicknose",
            "pie-eating",
            "racist",
            "elitist",
            "white trash",
            "drug-loving",
            "butterface",
            "tone-deaf",
            "ugly",
            "creepy"
            ]
        adjectives = [
            "douche",
            "ass ",
            "turd",
            "rectum",
            "butt",
            "cock",
            "shit",
            "crotch",
            "bitch",
            "turd",
            "prick",
            "slut",
            "taint",
            "fuck",
            "dick",
            "boner",
            "shard",
            "nut",
            "sphincter"
            ]
        nouns = [
            "pilot",
            "canoe",
            "captain",
            "pirate",
            "hammer",
            "knob",
            "box",
            "jockey",
            "nazi",
            "waffle",
            "goblin",
            "blossum",
            "biscuit",
            "clown",
            "socket",
            "monster",
            "hound",
            "dragon",
            "balloon"
            ]
        if len(args) > 0:
            disallowed = ['@jmh', 'jmh', '@zeta', 'zeta']
            if args[0] in disallowed:
                insult = "Hey @{}! ".format(str(msg.frm).split("/")[-1])
            else:
                insult = "Hey {}! ".format(args[0])
        else:
            insult = ""
        insult += "You {0} {1} {2}".format(
            random.choice(verbs),
            random.choice(adjectives),
            random.choice(nouns)
            )
        return insult

    @botcmd(split_args_with=None)
    def weather(self, msg, args):
        # Closest I could get to the studio in these locations
        _weather_map = {
                'malmo': 'pws:IMALM12;Malmö, SE',
                'paris': 'pws:I94VINCE18;Paris, FR'
                }

        _api = '29587dd40ebd6612'
        _url = f'http://api.wunderground.com/api/{_api}/conditions/q/'
        # _location = 'DK/Copenhagen'
        if len(args) == 0:
            _location = _weather_map['malmo'].split(';')[0]
            _name = _weather_map['malmo'].split(';')[1]
        else:
            _location = _weather_map[args[0].lower()].split(';')[0]
            _name = _weather_map[args[0].lower()].split(';')[1]
        _url = _url + _location + '.json'
        res = requests.get(_url, verify=False)
        r = res.json().get("current_observation")
        if r['weather'].lower().find('cloud') >= 0:
            r['weather'] = r['weather'] + " :cloud:"
        elif r['weather'].lower().find('rain') >= 0:
            r['weather'] = r['weather'] + " :rain_cloud:"
        if datetime.datetime.fromtimestamp(int(r['observation_epoch'])) < datetime.datetime.now() - datetime.timedelta(minutes=30):
            r['weather'] = r['weather'] + " Last update too old: {}".format(datetime.datetime.fromtimestamp(int(r['observation_epoch'])).isoformat())
        return f"{_name} :: {r['temp_c']}°C :: {r['weather']}"

    @botcmd(split_args_with=None)
    def tgi(self, msg, args):
        """
        Keeping this around for testing. Weeks are 2017/2018
        offers = {
                50:menu[2],
                51:menu[0],
                1:menu[1],
                2:menu[2],
                3:menu[0],
                4:menu[1],
                5:menu[2],
                6:menu[0],
                7:menu[1],
                8:menu[2],
                9:menu[0]
                }

        """
        menu = {
            0: "Ribs!",
            1: "the Grill menu.",
            2: "Premium Burgers."
        }
        weeknr: int = datetime.date.today().isocalendar()[1]
        _pronoun = 'This'
        if len(args) > 0:
            if args[0].lower() == 'next':
                weeknr: int = weeknr+1
                _pronoun = 'Next'
        offer = menu[weeknr % 3]
        return f"{_pronoun} weeks offer is {offer}"

    @botcmd(split_args_with=None)
    def coins(self, msg, args):
        if len(args) > 0:
            allowed_coins = ['monero', 'zcash', 'bitcoin']
            if args[0] not in allowed_coins:
                return "I only understand " + ', '.join(allowed_coins)
            else:
                coin = args[0]
        else:
            coin = 'monero'
        _api_url = "https://api.coinmarketcap.com/v1/ticker/" + coin
        _api_url += "/?ref=widget&convert=" + 'SEK'
        cookies = dict(currency='SEK')
        page = requests.get(_api_url, cookies=cookies)
        if page.status_code != 200:
            return 'Sorry, the response from coinmarketcap was ' + page.status_code
        sek = float(page.json()[0]['price_sek'])
        usd = float(page.json()[0]['price_usd'])
        return f'{coin} :: ${usd:.2f} :: {sek:.2f}kr'

    @botcmd(split_args_with=None)
    def coin(self, msg, args):
        return "*I AM NOT LISTENING FOR COIN SIR* Ask pluraly.\n!coins <coin name>\n!coins monero"

    @botcmd()
    def swisdom(self, msg, args):
        responses = [
                r"To slide in on a shrimp sandwich :: Att glida in på en räkmacka",
                r"Don't come here and glide in on a shrimp sandwich :: Kom inte här och glid in på en räkmacka",
                r"There is no cow on the ice :: Det är ingen ko på isen",
                r"I will throw a goats eye on it :: Jag slår ett getöga på det",
                r"If there is room for the heart, there is room for the butt :: Finns det hjärterum, finns det stjärterum",
                r"Close shoots no hare :: Nära skjuter ingen hare",
                r"Someone has taken a dump in the blue cabinet :: Någon har skitit i det blå skåpe",
                r"Sour, said the fox :: Surt, sa räven",
                r"I suspect owls in the bog  :: Jag anar ugglor i mossen",
                r"Here lies a dog buried :: Här ligger en hund begraven",
                r"Sitting with my beard in the mailbox :: Jag sitter med skägget i brevlådan",
                r"To buy a pig in a poke :: Att köpa grisen i säcken",
                r"The wheel is spinning but the hamster is dead :: Hjulet snurrar men hamstern är död",
                r"To jump in crazy barrels :: Hoppa i galen tunna",
                r"We have reached the poodles core :: Vi har nått pudelns kärna",
                r"Pour water on a goose :: Hälla vatten på en gås",
                r"Sour, said the fox about the rowan-berries :: Surt sa räven om rönnbären",
                r"""Don't say 'hi' until you're over the stream :: Säg inte 'hej' förrän du är över bäcken""",
                r"""That's how things go when the hook isn't on :: Så kan det gå när haspen inte är på""",
                r"""Empty barrels rumble the most :: Tomma tunnor bullrar mest""",
                r"""Sometimes the elevator doesn't go all the way to the top :: Ibland går inte hissen ända upp""",
                r"""The light is on, but no one is home :: Ljuset är på, men ingen är hemma""",
                r"""You don't miss the cow until the stable is empty :: Man saknar inte kon förrän båset är tomt""",
                r"""The one who doesn't have pants, has to walk around with a bare butt :: Den som inga byxor har, han får gå med ändan bar""",
                r"""walk like the cat around hot porridge :: gå som katten kring het gröt""",
                r"""throw yourself into the wall :: släng dig i väggen""",
                r"""pull me backwards :: Dra mig baklänges""",
                r"""Empty barrels rattle the most :: Tunna tunnor skramlar mest""",
                #r"""""",
                ]
        return random.choice(responses)
    @botcmd(split_args_with=None)
    def rule(self, msg, args):
        responses = [
                "", # rule #0
                "Do not talk about [REDACTED]",                                                     # rule #1
                "Do NOT talk about [REDACTED]",                                                     # rule #2
                "We are Anonymous",                                                                 # rule #3
                "Anonymous is legion",                                                              # rule #4
                "Anonymous never forgives",
                "Anonymous is a horrible, senseless, uncaring monster",
                "Anonymous is still able to deliver",
                "There are no real rules about posting",
                "There are no real rules about moderation either - enjoy your ban",                 
                "If you enjoy any rival sites - DON'T",                                             # rule #10
                "All your carefully picked arguments can easily be ignored",
                "Anything you say can and will be used against you",
                "Anything you say can be turned into something else",
                "Do not argue with trolls - it means they win",
                "The harder you try, the harder you will fail",
                "If you fail in epic proportions, it may just become a winning failure",
                "Every win fails eventually",
                "Everything that can be labeled can be hated",
                "The more you hate it the stronger it gets",
                "Nothing is to be taken seriously",
                "Original content is original only for a few seconds before getting old",
                "Copypasta is made to ruin every last bit of originality",
                "Copypasta is made to ruin every last bit of originality",
                "Every repost is always a repost of a repost",
                "Relation to the original topic decreases with every single post",
                "Any topic can be easily turned into something unrelated",
                "Always question a persons sexual preferences without any real reason",
                "Alyways question a persons gender - just in case it is really a man",
                "In the internet all girls are men and all kids are undercover FBI agents",
                "There are no girls on the internet",
                "TITS or GTFO - the choice is yours",
                "You must have pictures to prove your statements",
                "Lurk more - it's never enough",
                "There is porn of it, no exceptions ",
                "If no porn is found at the moment, it will be made ",
                "There will always be even more fucked up shit than what you just saw ",
                "You cannot divide by zero (just because the calculator says so) ",
                "No real limits of any kind apply here - not even the sky ",
                "CAPSLOCK IS CRUISE CONTROL FOR COOL ",
                "EVEN WITH CRUISE CONTROL YOU STILL HAVE TO STEER ",
                "Desu isn't funny. Seriously guys. It's worse than Chuck Norris jokes. ",
                "Nothing is Sacred ",
                "The more beautiful and pure a thing is - the more satisfying it is to corrupt it ",
                "Even one positive comment about Japanese things can make you a weeaboo ",
                "When one sees a lion, one must get into the car. ",
                "There is always furry porn of it. ",
                "The pool is always closed.", # END OF OFFICIAL RULES at 46
                "No matter what it is, Tony Stark can build it in his cave with a box of scraps.",
                "Everybody lies.",
                "Puns are always unfunny.",
                "One's USI increases with time spent on the internet.",
                "Everyone is gay for Bridget.",
                "Milhouse is not a meme.",
                "Leetspeak makes you cool.",
                "The OP is invariably a fag.",
                "The OP never delivers.",
                "God hates fags.",
                "TIME CUBE REIGNS SUPREME!",
                "There are dicks everywhere.",
                "8chan is the new 4chan.",
                "Everyone on the internet will claim to be an atheist.",
                "It's always rape.",
                "On the internet, no one can hear you get raped.",
                "There is always a female version of a male character, and vice versa (Note: This rule is already official. It's only here because the official rule list only goes up to 47. Retarded, amirite?)",
                "Every meme always becomes an old meme.",
                "Every old meme always becomes a dead meme.",
                "Don't divide by zero.",
                "The truth is always painful.",
                "If you're looking at an incomprehensible image of something that is the same colour as any kind of human skin, your brain is warning you that you don't want to know what it really is. This goes double if there is even a spot of red coloration in the image.",
                "No matter how many proxies you hide behind, you are always tracable.",
                "Ships are based on stereotypes, fettishes, and how cute interactions are, not actual love."
                ]
        try:
            return responses[int(args[0].strip("#"))]
        except IndexError:
            return "Please tell me which rule you'd like to list.."
        except ValueError:
            return "That doesn't look like a number to me."
